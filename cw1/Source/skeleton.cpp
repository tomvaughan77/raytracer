#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModelH.h"
#include <stdint.h>
#include <limits.h>
#include <math.h>
#include <omp.h>

using namespace std;
using glm::vec3;
using glm::mat3;
using glm::vec4;
using glm::mat4;

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 256
#define FULLSCREEN_MODE false
#define AA_RES 4
#define FOCAL_LENGTH 2.75
#define DOF_RES 1

struct Intersection {
  vec4 position;
  float distance;
  int triangleIndex;
};

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

void Update();
void UpdateR(mat4 R, vec4 theta);
void Draw(screen* screen);
bool ClosestIntersection(vec4 start,
                         vec4 dir,
                         const vector<Triangle>& triangles,
                         Intersection& ClosestIntersection);
vec3 DirectLight(const Intersection& i);

vector<Triangle> triangles;
const float m = std::numeric_limits<float>::max();
const float focalLength = 1.0;
vec4 cameraPos(0.0, 0.0, -3.0, 1.0);
vec4 theta(0.0, 0.0, 0.0, 1.0);
mat4 R;
vec4 lightPos(0.0, -0.5, -0.7, 1.0);
const vec3 lightColour = 14.f * vec3(1.0, 1.0, 1.0);
const vec3 indirectLight = 0.5f * vec3(1.0, 1.0, 1.0);

int main( int argc, char* argv[] )
{
  screen *screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );

  LoadTestModel(triangles);

  while( NoQuitMessageSDL() )
    {
      Update();
      Draw(screen);
      SDL_Renderframe(screen);
    }

  SDL_SaveImage( screen, "screenshot.bmp" );

  KillSDL(screen);
  return 0;
}

vec3 DirectLight(const Intersection& i) {
  vec4 n;
  vec4 lightdir;
  vec3 D;
  vec3 B;
  Intersection j;

  //Set normal to normal vector out of triangles
  n = triangles[i.triangleIndex].normal;

  vec4 bias_i = i.position + n * 0.0001f;

  //Set lightdir as vector between lightPos and i.position
  lightdir = vec4((lightPos.x - bias_i.x),
                  (lightPos.y - bias_i.y),
                  (lightPos.z - bias_i.z),
                  (1.0));

  //Calculate B as P/A using power from lightColour
  B = lightColour / (float)(4 * M_PI * glm::length(lightdir) * glm::length(lightdir));

  if (dot(n, lightdir) > 0) {
    //See if there are any intersections between lightPos and target
    ClosestIntersection(bias_i, glm::normalize(lightdir), triangles, j);

    if (j.distance > glm::length(lightdir)) {
      D = B * dot(n, lightdir);
    }
    else {
      D = vec3(0.0, 0.0, 0.0);
    }
  }
  else {
    D = vec3(0.0, 0.0, 0.0);
  }

  return D;
}

bool ClosestIntersection(vec4 start, vec4 dir, const vector<Triangle>& triangles, Intersection& closestIntersection) {

  bool result = false;
  Intersection smallest;
  smallest.distance = m;
  smallest.triangleIndex = INT_MAX;

  for (size_t i=0; i<triangles.size(); i++) {
    vec4 v0 = triangles[i].v0;
    vec4 v1 = triangles[i].v1;
    vec4 v2 = triangles[i].v2;

    vec3 e1 = vec3(v1.x-v0.x, v1.y-v0.y, v1.z-v0.z);
    vec3 e2 = vec3(v2.x-v0.x, v2.y-v0.y, v2.z-v0.z);
    vec3 b  = vec3(start.x-v0.x, start.y-v0.y, start.z-v0.z);

    vec3 d = vec3(dir.x, dir.y, dir.z);

    mat3 A(-d, e1, e2);

    float detA = glm::determinant(A);

    float detA0 = glm::determinant(mat3(b, A[1], A[2]));
    float detA1 = glm::determinant(mat3(A[0], b, A[2]));
    float detA2 = glm::determinant(mat3(A[0], A[1], b));

    vec3 x = vec3(
      detA0 / detA,
      detA1 / detA,
      detA2 / detA
    );

    float t = x.x;
    float u = x.y;
    float v = x.z;

    if (u >= 0 && v >= 0 && (u + v) <= 1 && t > 0) {
      result = true;
      if (t < smallest.distance) {
        smallest.position = start + (t * dir);
        smallest.distance = t;
        smallest.triangleIndex = i;
      }
    }
  }

  if (result) {
    closestIntersection = smallest;
  }
  else {
    closestIntersection.distance = m;
  }

  return result;
}


void GaussianBlur (vec3 pre[SCREEN_WIDTH][SCREEN_HEIGHT]) {
  vec3 accumulator[SCREEN_WIDTH][SCREEN_HEIGHT];
  float kernel[5];
  kernel[0] = 1;
  kernel[1] = 4;
  kernel[2] = 6;
  kernel[3] = 4;
  kernel[4] = 1;

  for (int i = 0; i < SCREEN_WIDTH; i++) {
    for (int j = 0; j < SCREEN_HEIGHT; j++) {
      accumulator[i][j].x = 0;
      accumulator[i][j].y = 0;
      accumulator[i][j].z = 0;

      for (int m = 0; m < 5; m++) {
        for (int n = 0; n < 5; n++) {
          if (i-2+m >= 0 && j-2+n >= 0 && i-2+m <= SCREEN_WIDTH && j-2+n <= SCREEN_HEIGHT) {
            accumulator[i][j].x += pre[i-2+m][j-2+n].x*kernel[m]*kernel[n];
            accumulator[i][j].y += pre[i-2+m][j-2+n].y*kernel[m]*kernel[n];
            accumulator[i][j].z += pre[i-2+m][j-2+n].z*kernel[m]*kernel[n];
          }
          else {
            accumulator[i][j].x += pre[i][j].x*kernel[m]*kernel[n];
            accumulator[i][j].y += pre[i][j].y*kernel[m]*kernel[n];
            accumulator[i][j].z += pre[i][j].z*kernel[m]*kernel[n];
          }
        }
      }
    }
  }

  for (int i = 0; i < SCREEN_WIDTH; i++) {
    for (int j = 0; j < SCREEN_HEIGHT; j++) {
      pre[i][j] = accumulator[i][j]/vec3(256, 256, 256);
    }
  }
}


void AntiAliasing (  vec3 sharp     [SCREEN_WIDTH][SCREEN_HEIGHT],
                     float distances[SCREEN_WIDTH][SCREEN_HEIGHT],
                     mat4 R) {
  const vec4 right  (R[0][0], R[0][1], R[0][2], 1.0);
  const vec4 down   (R[1][0], R[1][1], R[1][2], 1.0);
  const vec4 forward(R[2][0], R[2][1], R[2][2], 1.0);

  #pragma omp parallel for
  for (int i=0; i<SCREEN_WIDTH; i++) {
    Intersection closestIntersection;
    for (int j=0; j<SCREEN_HEIGHT; j++) {
      vec3 newColour = vec3(0.0, 0.0, 0.0);
      for (int incI = 0; incI < AA_RES; incI++) {
        for (int incJ = 0; incJ < AA_RES; incJ++) {
          vec4 dir = vec4(
            (i*AA_RES+incI - SCREEN_WIDTH*AA_RES/2)/(float)SCREEN_WIDTH/(AA_RES),
            (j*AA_RES+incJ - SCREEN_HEIGHT*AA_RES/2)/(float)SCREEN_HEIGHT/(AA_RES),
            focalLength,
            0.0
          );
          vec4 newDir = vec4(dot(dir, right), dot(dir, down), dot(dir, forward), 1.0);

          ClosestIntersection(cameraPos, newDir, triangles, closestIntersection);

          vec3 D = DirectLight(closestIntersection);

          if (closestIntersection.distance != m) {
            newColour += triangles[closestIntersection.triangleIndex].color * (D + indirectLight);
            distances[i][j] = closestIntersection.distance;
          }
          else {
            distances[i][j] = -1;
          }
        }
      }
      float squared = AA_RES*AA_RES;
      vec3 R = newColour/vec3(squared, squared, squared);
      sharp[i][j]   = R;
    }
  }
}


void DepthOfFieldFragments (vec3 blurred[SCREEN_WIDTH][SCREEN_HEIGHT])
{
  vec3 sharp     [SCREEN_WIDTH][SCREEN_HEIGHT];
  float distances[SCREEN_WIDTH][SCREEN_HEIGHT];
  for (int i = -DOF_RES; i < DOF_RES; i++) {
    for (int j = -DOF_RES; j < DOF_RES; j++) {
      mat4 tempR = R;
      vec4 tempTheta = theta;
      tempTheta.x+=i*20;
      tempTheta.y+=j*20;
      UpdateR(tempR, tempTheta);
      GaussianBlur(sharp);
      AntiAliasing(sharp, distances, tempR);
      float dof_squared = (DOF_RES*2+1)*1.25;
      for (int i=0; i<SCREEN_WIDTH; i++) {
        for (int j=0; j<SCREEN_HEIGHT; j++) {
          blurred[i][j] += sharp[i][j]/dof_squared;
        }
      }
    }
  }
  GaussianBlur(blurred);
}


/*Place your drawing here*/
void Draw(screen* screen)
{
  /* Clear buffer */
  memset(screen->buffer, 0, screen->height*screen->width*sizeof(uint32_t));

  vec3 sharp     [SCREEN_WIDTH][SCREEN_HEIGHT];
  vec3 blurred   [SCREEN_WIDTH][SCREEN_HEIGHT];
  float distances[SCREEN_WIDTH][SCREEN_HEIGHT];

  AntiAliasing(sharp, distances, R);



  DepthOfFieldFragments (blurred);

  /*for (int i=0; i<SCREEN_WIDTH; i++) {
    for (int j=0; j<SCREEN_HEIGHT; j++) {
      PutPixelSDL(screen, i, j, blurred[i][j]);
    }
  }*/

  for (int i=0; i<SCREEN_WIDTH; i++) {
    for (int j=0; j<SCREEN_HEIGHT; j++) {

      if (distances[i][j] >= FOCAL_LENGTH && distances[i][j] <= FOCAL_LENGTH+0.5) {
        float modifier = (distances[i][j]-FOCAL_LENGTH)*2;
        vec3 interpolatedPixel = blurred[i][j]*vec3(modifier, modifier, modifier) + sharp[i][j]*vec3(1-modifier,1-modifier,1-modifier);
        PutPixelSDL(screen, i, j, interpolatedPixel);
        //PutPixelSDL(screen, i, j, vec3(0,0,0));
      }
      else if (distances[i][j] >= FOCAL_LENGTH-0.5 && distances[i][j] <= FOCAL_LENGTH) {
        float modifier = (distances[i][j]-FOCAL_LENGTH+0.5)*2;
        vec3 interpolatedPixel = blurred[i][j]*vec3(1-modifier, 1-modifier, 1-modifier) + sharp[i][j]*vec3(modifier,modifier,modifier);
        PutPixelSDL(screen, i, j, interpolatedPixel);
        //PutPixelSDL(screen, i, j, vec3(0,0,0));
      }
      else {
        PutPixelSDL(screen, i, j, blurred[i][j]);
      }

      /*if (distances[i][j] <= FOCAL_LENGTH) {
        PutPixelSDL(screen, i, j, sharp[i][j]);
      }
      else if (distances[i][j] > FOCAL_LENGTH+1) {
        PutPixelSDL(screen, i, j, blurred[i][j]);
      }
      else {
        float modifier = (distances[i][j]-FOCAL_LENGTH)/((FOCAL_LENGTH+1)/2);
        vec3 interpolatedPixel = blurred[i][j]*vec3(modifier, modifier, modifier) + sharp[i][j]*vec3(1-modifier,1-modifier,1-modifier);
        PutPixelSDL(screen, i, j, interpolatedPixel);
      }*/
    }
  }
}

void UpdateR(mat4 R, vec4 theta) {
  float x = theta.x;
  float y = theta.y;
  float z = theta.z;

  R[0][0] = cos(y)*cos(z);
  R[0][1] = -cos(x)*sin(z)+sin(x)*sin(y)*cos(z);
  R[0][2] = sin(x)*sin(z)+cos(x)*sin(y)*cos(z);
  R[0][3] = cameraPos.x;
  R[1][0] = cos(y)*sin(z);
  R[1][1] = cos(x)*cos(z)+sin(x)*sin(y)*sin(z);
  R[1][2] = -sin(x)*cos(z)+cos(x)*sin(y)*sin(z);
  R[1][3] = cameraPos.y;
  R[2][0] = -sin(y);
  R[2][1] = sin(x)*cos(y);
  R[2][2] = cos(x)*cos(y);
  R[2][3] = cameraPos.z;
  R[3][0] = 0.0;
  R[3][1] = 0.0;
  R[3][2] = 0.0;
  R[3][3] = 1.0;

  /*mat4 matIn = mat4(1.0f);
  matIn[0][3] = cameraPos.x;
  matIn[1][3] = cameraPos.y;
  matIn[2][3] = cameraPos.z;

  mat4 matOut = mat4(1.0f);
  matOut[0][3] = -cameraPos.x;
  matOut[1][3] = -cameraPos.y;
  matOut[2][3] = -cameraPos.z;*/
}

/*Place updates of parameters here*/
void Update()
{
  //static int t = SDL_GetTicks();
  // Compute frame time
  //int t2 = SDL_GetTicks();
  //float dt = float(t2-t);
  //t = t2;
  /*Good idea to remove this*/
  //std::cout << "Render time: " << dt << " ms." << std::endl;
  /* Update variables*/

  const Uint8* keystate = SDL_GetKeyboardState(NULL);
  if (keystate[SDL_SCANCODE_W]) { //Y
    cameraPos.y -= 0.05;
  }
  if (keystate[SDL_SCANCODE_S]) {
    cameraPos.y += 0.05;
  }
  if (keystate[SDL_SCANCODE_A]) { //X
    cameraPos.x -= 0.05;
  }
  if (keystate[SDL_SCANCODE_D]) {
    cameraPos.x += 0.05;
  }
  if (keystate[SDL_SCANCODE_Q]) { //Z
    cameraPos.z += 0.05;
  }
  if (keystate[SDL_SCANCODE_E]) {
    cameraPos.z -= 0.05;
  }
  if (keystate[SDL_SCANCODE_UP]) { //Y
    theta.x += 0.05;
    UpdateR(R, theta);
  }
  if (keystate[SDL_SCANCODE_DOWN]) {
    theta.x -= 0.05;
    UpdateR(R, theta);
  }
  if (keystate[SDL_SCANCODE_LEFT]) { //X
    theta.y -= 0.05;
    UpdateR(R, theta);
  }
  if (keystate[SDL_SCANCODE_RIGHT]) {
    theta.y += 0.05;
    UpdateR(R, theta);
  }
  if (keystate[SDL_SCANCODE_Z]) { //Z
    theta.z += 0.05;
    UpdateR(R, theta);
  }
  if (keystate[SDL_SCANCODE_X]) {
    theta.z -= 0.05;
    UpdateR(R, theta);
  }
  if (keystate[SDL_SCANCODE_I]) { //Y
    lightPos.y -= 0.05;
  }
  if (keystate[SDL_SCANCODE_K]) {
    lightPos.y += 0.05;
  }
  if (keystate[SDL_SCANCODE_J]) { //X
    lightPos.x -= 0.05;
  }
  if (keystate[SDL_SCANCODE_L]) {
    lightPos.x += 0.05;
  }
  if (keystate[SDL_SCANCODE_U]) { //Z
    lightPos.z += 0.05;
  }
  if (keystate[SDL_SCANCODE_O]) {
    lightPos.z -= 0.05;
  }
}
