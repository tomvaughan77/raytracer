#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModelH.h"
#include <stdint.h>

using namespace std;
using glm::vec2;
using glm::vec3;
using glm::mat3;
using glm::vec4;
using glm::mat4;
using glm::ivec2;


#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 256
#define FULLSCREEN_MODE false

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

void VertexShader(const vec4& v, ivec2& p);
void Interpolate(ivec2 a, ivec2 b, vector<ivec2>& result);
void DrawLineSDL(screen* screen, ivec2 a, ivec2 b, vec3 colour);
void DrawPolygonEdges(screen* screen, const vector<vec4>& verticies);
void updateR();
void Update();
void Draw(screen* screen);

vec4 cameraPos(0.0, 0.0, -3.0, 1.0);
float focalLength = 0.0;
vector<Triangle> triangles;
mat4 R;
vec4 theta(0.0, 0.0, 0.0, 1.0);

int main( int argc, char* argv[] )
{

  screen *screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );

  LoadTestModel(triangles);

  while( NoQuitMessageSDL() )
    {
      Update();
      Draw(screen);
      SDL_Renderframe(screen);
    }

  SDL_SaveImage( screen, "screenshot.bmp" );

  KillSDL(screen);
  return 0;
}

void updateR() {
  float x = theta.x;
  float y = theta.y;
  float z = theta.z;

  R[0][0] = cos(y)*cos(z);
  R[0][1] = -cos(x)*sin(z)+sin(x)*sin(y)*cos(z);
  R[0][2] = sin(x)*sin(z)+cos(x)*sin(y)*cos(z);
  R[0][3] = cameraPos.x;
  R[1][0] = cos(y)*sin(z);
  R[1][1] = cos(x)*cos(z)+sin(x)*sin(y)*sin(z);
  R[1][2] = -sin(x)*cos(z)+cos(x)*sin(y)*sin(z);
  R[1][3] = cameraPos.y;
  R[2][0] = -sin(y);
  R[2][1] = sin(x)*cos(y);
  R[2][2] = cos(x)*cos(y);
  R[2][3] = cameraPos.z;
  R[3][0] = 0.0;
  R[3][1] = 0.0;
  R[3][2] = 0.0;
  R[3][3] = 1.0;
}

void VertexShader(const vec4& v, ivec2& p) {
  vec4 transformed = v - cameraPos;
  transformed = R * transformed;
  p.x = focalLength * ((transformed.x / transformed.z) + (SCREEN_WIDTH / 2));
  cout << "VS: " << transformed.x << " / " << transformed.z << " = " << (transformed.x / transformed.z) << "\n";
  p.y = focalLength * ((transformed.y / transformed.z) + (SCREEN_HEIGHT / 2));
}

/*void Interpolate(vec2 a, vec2 b, vector<vec2>& result) {
  vec2 interval;
  vec2 counter;
  counter = a;

  if (result.size() == 1) {
    interval.x = abs(a.x - b.x);
    interval.y = abs(a.y - b.y);
  }
  else {
    interval.x = abs(a.x - b.x) / (result.size() - 1);
    interval.y = abs(a.y - b.y) / (result.size() - 1);
  }

  if (a.x > b.x) interval.x = -interval.x;
  if (a.y > b.y) interval.y = -interval.y;

  for (size_t i=0; i<result.size(); i++) {
    result[i] = counter;
    counter += interval;
  }
}*/

void Interpolate( ivec2 a, ivec2 b, vector<ivec2>& result ) {
  int N = result.size();
  vec2 step = vec2(b-a) / float(max(N-1,1));
  vec2 current = vec2(a);
  for( int i=0; i<N; ++i ) {
    result[i] = current;
    current += step;
  }
}

void DrawLineSDL(screen* screen, ivec2 a, ivec2 b, vec3 colour) {
  ivec2 delta = glm::abs(a - b);
  int pixels = glm::max(delta.x, delta.y) + 1;

  vector<ivec2> line(pixels);
  Interpolate(a, b, line);

  for (size_t i=0; i<line.size(); i++) {
    PutPixelSDL(screen, line[i].x, line[i].y, colour);
  }

}

void DrawPolygonEdges(screen* screen, const vector<vec4>& verticies) {
  int V = verticies.size();
  vector<ivec2> projectedVerticies(V);
  for (int i=0; i<V; i++) {
    VertexShader(verticies[i], projectedVerticies[i]);
    //cout << "VERTS " << verticies[i].x << " " << verticies[i].y << " " << verticies[i].z << "\n";
    cout << "PROJVERTS " << projectedVerticies[i].x << " " << projectedVerticies[i].y << "\n";
  }

  for (int i=0; i<V; i++) {
    int j = (i + 1) % V;
    vec3 colour(1.0, 1.0, 1.0);
    DrawLineSDL(screen, projectedVerticies[i], projectedVerticies[j], colour);
  }
}


/*Place your drawing here*/
void Draw(screen* screen)
{
  /* Clear buffer */
  memset(screen->buffer, 0, screen->height*screen->width*sizeof(uint32_t));

  for (uint32_t i=0; i<triangles.size(); i++) {

    cout << "TRIANGLE SIZE: " << triangles.size() << "\n";
    cout << "SIZES: " << SCREEN_WIDTH << " " << SCREEN_HEIGHT << "\n";

    vector<vec4> verticies(3);
    verticies[0] = triangles[i].v0;
    verticies[1] = triangles[i].v1;
    verticies[2] = triangles[i].v2;

    for (int j=0; j<3; j++) {
        cout << "VERTS " << verticies[j].x << " " << verticies[j].y << " " << verticies[j].z << "\n";
    }

    DrawPolygonEdges(screen, verticies);

    cout << "[" << i << "]#########################\n\n";

    //cout << "CAMERA " << cameraPos.x << " " << cameraPos.y << " " << cameraPos.z << "\n";

  }
}

/*Place updates of parameters here*/
void Update()
{
  static int t = SDL_GetTicks();
  /* Compute frame time */
  int t2 = SDL_GetTicks();
  float dt = float(t2-t);
  t = t2;
  /*Good idea to remove this*/
  //std::cout << "Render time: " << dt << " ms." << std::endl;
  /* Update variables*/

  const Uint8* keystate = SDL_GetKeyboardState(NULL);
  if (keystate[SDL_SCANCODE_W]) { //Y
    cameraPos.y -= 0.05;
  }
  if (keystate[SDL_SCANCODE_S]) {
    cameraPos.y += 0.05;
  }
  if (keystate[SDL_SCANCODE_A]) { //X
    cameraPos.x -= 0.05;
  }
  if (keystate[SDL_SCANCODE_D]) {
    cameraPos.x += 0.05;
  }
  if (keystate[SDL_SCANCODE_Q]) { //Z
    cameraPos.z += 0.05;
  }
  if (keystate[SDL_SCANCODE_E]) {
    cameraPos.z -= 0.05;
  }
  if (keystate[SDL_SCANCODE_UP]) { //Y
    theta.x += 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_DOWN]) {
    theta.x -= 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_LEFT]) { //X
    theta.y -= 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_RIGHT]) {
    theta.y += 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_Z]) { //Z
    theta.z += 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_X]) {
    theta.z -= 0.05;
    updateR();
  }


  while (theta.x < 0) {
    theta.x += 2*M_PI;
  }
  while (theta.y < 0) {
    theta.y += 2*M_PI;
  }
  while (theta.z < 0) {
    theta.z += 2*M_PI;
  }
  while (theta.x > M_PI*2) {
    theta.x -= 2*M_PI;
  }
  while (theta.y > M_PI*2) {
    theta.y -= 2*M_PI;
  }
  while (theta.z > M_PI*2) {
    theta.z -= 2*M_PI;
  }

}
