#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModelH.h"
#include <stdint.h>
#include <omp.h>

using namespace std;
using glm::vec2;
using glm::ivec2;
using glm::vec3;
using glm::mat3;
using glm::vec4;
using glm::mat4;

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 256
#define FULLSCREEN_MODE false
#define FOCAL_LENGTH 256
#define AA_RES 15

struct Pixel {
  ivec2 pos;
  float zinv;
  vec4 pos3d;
};

struct Vertex {
  vec4 pos;
};

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

void Update();
void Draw();
void Interpolate(Pixel a, Pixel b, vector<Pixel>& result);
void ComputePolygonRows(const vector<Pixel>& vertexPixels
                      , vector<Pixel>& leftPixels
                      , vector<Pixel>& rightPixels);
void DrawPolygonRows(const vector<Pixel>& leftPixels
                      , vector<Pixel>& rightPixels
                      , const vec3 colour);
void DrawPolygon(const vector<vec4>& vertices
                      , const vec3 colour);
vec4 MatrixTransform (vec4 p);
void VertexShader (Pixel& p, const vec4& v);
void PixelShader(const Pixel& p, const vec3 colour);
void updateR();
void DrawPixels (screen* screen);


/* ----------------------------------------------------------------------------*/
/* GLOBALS                                                                     */
vector<Triangle> triangles;
mat4 R;
vec4 cameraPos(0.0, 0.0, -3.001, 1.0);
vec4 theta    (0.0, 0.0,    0.0, 1.0);
float depthBuffer[SCREEN_WIDTH*2][SCREEN_HEIGHT*2];
vec4 lightPos(0,-0.5,-0.7, 1.0);
vec3 lightPower = 14.0f*vec3( 1, 1, 1 );
vec3 indirectLightPowerPerArea = 0.5f*vec3( 1, 1, 1 );
vec4 currentNormal;
vec3 currentReflectance;
vec3 image[SCREEN_WIDTH][SCREEN_HEIGHT][AA_RES];
int screen_index;

int main( int argc, char* argv[] )
{

  screen *screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );
  LoadTestModel(triangles);
  float cameraWiggle = 0.0175 / AA_RES;

  while( NoQuitMessageSDL() )
    {
      Update();
      screen_index = 0;
      vec4 camTemp = cameraPos;
      //#pragma omp parallel for
      for (int j=0; j<AA_RES; j++) {
        camTemp.x += cameraWiggle * j;
        camTemp.y += cameraWiggle * j;
        camTemp.z += cameraWiggle * j;
        Draw();
        screen_index = j;
      }
      camTemp.x -= AA_RES * cameraWiggle;
      camTemp.y -= AA_RES * cameraWiggle;
      camTemp.z -= AA_RES * cameraWiggle;
      vec3 avg;
      for (int i=0; i<SCREEN_WIDTH; i++) {
        for (int j=0; j<SCREEN_HEIGHT; j++) {
          avg = vec3(0, 0, 0);
          for (int k=0; k<AA_RES; k++) {
            avg += image[i][j][k];
            image[i][j][k] = vec3(0, 0, 0);
          }
          avg.x = avg.x / AA_RES;
          avg.y = avg.y / AA_RES;
          avg.z = avg.z / AA_RES;
          PutPixelSDL(screen, i, j, avg);
        }
      }
      SDL_Renderframe(screen);
    }

  SDL_SaveImage( screen, "screenshot.bmp" );

  KillSDL(screen);
  return 0;
}

/*Place your drawing here*/
void Draw()
{
  /* Clear buffer */
  //memset(screen->buffer, 0, screen->height*screen->width*sizeof(uint32_t));

  for (int x=0; x<SCREEN_WIDTH*2; x++) {
    for (int y=0; y<SCREEN_HEIGHT*2; y++) {
      depthBuffer[x][y] = 0;
    }
  }

  for (int x=0; x<SCREEN_WIDTH; x++){
    for (int y=0; y<SCREEN_HEIGHT; y++) {
      pixelValues[x][y] = vec3(0,0,0);
    }
  }

  for (int i = 0; i<(int)triangles.size(); ++i) {
    vector<vec4> vertices(3);

    currentNormal = triangles[i].normal;
    currentReflectance = vec3(1.0, 1.0, 1.0);

    vertices[0] = triangles[i].v0;
    vertices[1] = triangles[i].v1;
    vertices[2] = triangles[i].v2;

    DrawPolygon(vertices, triangles[i].color);
  }
}

void DrawPolygon(const vector<vec4>& vertices, const vec3 colour) {
  int V = vertices.size();
  vector<Pixel> vertexPixels(V);
  for (int i=0; i<V; i++) {
    VertexShader(vertexPixels[i], vertices[i]);
  }

  vector<Pixel> leftPixels;
  vector<Pixel> rightPixels;
  ComputePolygonRows(vertexPixels, leftPixels, rightPixels);
  DrawPolygonRows(leftPixels, rightPixels, colour);
}

void VertexShader (Pixel& p, const vec4& v) {
  vec4 transformed = R*(v-cameraPos);

  p.zinv = 1.0/float(transformed.z);
  p.pos.x = FOCAL_LENGTH*transformed.x/transformed.z + SCREEN_WIDTH/2;
  p.pos.y = FOCAL_LENGTH*transformed.y/transformed.z + SCREEN_HEIGHT/2;
  p.pos3d = transformed;
}

void PixelShader(const Pixel& p, const vec3 colour) {
  vec3 D;
  vec3 R;
  vec3 B;
  vec4 lightdir;

  lightdir = vec4((lightPos.x - p.pos3d.x),
                  (lightPos.y - p.pos3d.y),
                  (lightPos.z - p.pos3d.z),
                  (1.0));

  D = (lightPower * max(dot(lightdir, currentNormal), 0.f)) / (float)(4 * M_PI * glm::length(lightdir) * glm::length(lightdir));

  R = D + indirectLightPowerPerArea;
  R = colour * R;
  //cout << "R: " << p.pos3d.x << " " << p.pos3d.y << " " << p.pos3d.z << endl;


  if (p.pos.x < SCREEN_WIDTH*2 && p.pos.x >= 0 && p.pos.y < SCREEN_HEIGHT*2 && p.pos.y >= 0) {
    if (p.zinv > depthBuffer[p.pos.x][p.pos.y]) {
      depthBuffer[p.pos.x][p.pos.y] = p.zinv;
      //PutPixelSDL(screen, p.pos.x, p.pos.y, R);
      image[p.pos.x][p.pos.y][screen_index] = R;
    }
    /*else {
      image[p.pos.x][p.pos.y][screen_index] = vec3(0, 0, 0);
    }*/
  }
}

void ComputePolygonRows(const vector<Pixel>& vertexPixels
                      , vector<Pixel>& leftPixels
                      , vector<Pixel>& rightPixels) {
  Pixel max, min, mid;
  max.pos = ivec2(INT_MIN, INT_MIN);
  max.zinv = 0;
  min.pos = ivec2(INT_MAX, INT_MAX);
  min.zinv = 0;
  mid.pos = max.pos;
  mid.zinv = 0;


  for (size_t i=0; i<vertexPixels.size(); i++) {
    if (vertexPixels[i].pos.y > max.pos.y) {
      max.pos = vertexPixels[i].pos;
      max.zinv = vertexPixels[i].zinv;
      max.pos3d = vertexPixels[i].pos3d;
    }
  }

  for (size_t i=0; i<vertexPixels.size(); i++) {
    if (vertexPixels[i].pos.y < min.pos.y) {
      min.pos = vertexPixels[i].pos;
      min.zinv = vertexPixels[i].zinv;
      min.pos3d = vertexPixels[i].pos3d;
    }
  }

  for (int i=0; i<(int)vertexPixels.size(); i++) {
    if (vertexPixels[i].pos != max.pos && vertexPixels[i].pos != min.pos) {
      mid.pos = vertexPixels[i].pos;
      mid.zinv = vertexPixels[i].zinv;
      mid.pos3d = vertexPixels[i].pos3d;
    }
  }

  int height = abs(max.pos.y - min.pos.y + 1);

  leftPixels.resize(height);
  rightPixels.reserve(height);

  vector<Pixel> temp_top(abs(mid.pos.y-min.pos.y)+1);
  vector<Pixel> temp_bot(abs(max.pos.y-mid.pos.y)+1);

  Interpolate(min, max, leftPixels);
  Interpolate(min, mid, temp_top);
  Interpolate(mid, max, temp_bot);

  for (int i=0; i<(int)(temp_top.size() - 1); i++) {
    rightPixels.push_back(temp_top[i]);
  }
  for (int i=0; i<(int)temp_bot.size(); i++) {
    rightPixels.push_back(temp_bot[i]);
  }

  if (leftPixels[ceil(height / 2)].pos.x > rightPixels[(height / 2)].pos.x) {
    temp_top.resize(height);
    temp_top = leftPixels;
    leftPixels = rightPixels;
    rightPixels = temp_top;
  }
}

void DrawPolygonRows(const vector<Pixel>& leftPixels, vector<Pixel>& rightPixels, const vec3 colour) {
  for (int r=0; r<(int)leftPixels.size(); r++) {
    vector<Pixel> row(abs(leftPixels[r].pos.x - rightPixels[r].pos.x));
    rightPixels[r].pos.x -= 1;
    Interpolate(leftPixels[r], rightPixels[r], row);
    for (int i=0; i<(int)row.size(); i++) {
      PixelShader(row[i], colour);
    }
  }
}

void Interpolate(Pixel a, Pixel b, vector<Pixel>& result) {
  int N = result.size();
  vec2 stepPos = vec2(b.pos-a.pos) / float(max(N-1,1));
  float stepDepth = (b.zinv-a.zinv) / float(max(N-1,1));
  vec3 stepPos3d = vec3(b.pos3d - a.pos3d) / float(max(N-1,1));
  vec2 currentPos(a.pos);
  float currentDepth = a.zinv;
  vec4 currentPos3d = a.pos3d;
  for( int i=0; i<N; ++i ) {
    result[i].pos = currentPos;
    result[i].zinv = currentDepth;
    result[i].pos3d = currentPos3d;
    currentPos += stepPos;
    currentDepth += stepDepth;
    currentPos3d.x += stepPos3d.x;
    currentPos3d.y += stepPos3d.y;
    currentPos3d.z += stepPos3d.z;
  }
}

// Rotation borked big-time
vec4 MatrixTransform (vec4 p) {
  vec4 c = cameraPos;
  mat4 r = mat4     (R[0][0], R[0][1], R[0][2], 0
                   , R[1][0], R[1][1], R[1][2], 0
                   , R[2][0], R[2][1], R[2][2], 0
                   ,       0,       0,       0, 1);
  mat4 c_pos = mat4 (0, 0, 0,  -c.x
                   , 0, 0, 0,  -c.y
                   , 0, 0, 0,  -c.z
                   , 0, 0, 0,    1);
  mat4 c_neg = mat4 (0, 0, 0, c.x
                   , 0, 0, 0, c.y
                   , 0, 0, 0, c.z
                   , 0, 0, 0,    1);
  return transpose(c_pos)*r*transpose(c_neg)*p;
}


void updateR() {
  float x = theta.x;
  float y = theta.y;
  float z = theta.z;

  R[0][0] = cos(y)*cos(z);
  R[0][1] = -cos(x)*sin(z)+sin(x)*sin(y)*cos(z);
  R[0][2] = sin(x)*sin(z)+cos(x)*sin(y)*cos(z);
  R[0][3] = cameraPos.x;
  R[1][0] = cos(y)*sin(z);
  R[1][1] = cos(x)*cos(z)+sin(x)*sin(y)*sin(z);
  R[1][2] = -sin(x)*cos(z)+cos(x)*sin(y)*sin(z);
  R[1][3] = cameraPos.y;
  R[2][0] = -sin(y);
  R[2][1] = sin(x)*cos(y);
  R[2][2] = cos(x)*cos(y);
  R[2][3] = cameraPos.z;
  R[3][0] = 0.0;
  R[3][1] = 0.0;
  R[3][2] = 0.0;
  R[3][3] = 1.0;
}


/*Place updates of parameters here*/
void Update()
{
  static int t = SDL_GetTicks();
  //Compute frame time
  int t2 = SDL_GetTicks();
  float dt = float(t2-t);
  t = t2;
  //Good idea to remove this
  std::cout << "Render time: " << dt << " ms." << std::endl;
  /* Update variables*/

  const Uint8* keystate = SDL_GetKeyboardState(NULL);
  if (keystate[SDL_SCANCODE_W]) { //Y
    cameraPos.y -= 0.05;
  }
  if (keystate[SDL_SCANCODE_S]) {
    cameraPos.y += 0.05;
  }
  if (keystate[SDL_SCANCODE_A]) { //X
    cameraPos.x -= 0.05;
  }
  if (keystate[SDL_SCANCODE_D]) {
    cameraPos.x += 0.05;
  }
  if (keystate[SDL_SCANCODE_Q]) { //Z
    cameraPos.z += 0.05;
  }
  if (keystate[SDL_SCANCODE_E]) {
    cameraPos.z -= 0.05;
  }
  if (keystate[SDL_SCANCODE_UP]) { //Y
    theta.x += 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_DOWN]) {
    theta.x -= 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_LEFT]) { //X
    theta.y -= 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_RIGHT]) {
    theta.y += 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_Z]) { //Z
    theta.z += 0.05;
    updateR();
  }
  if (keystate[SDL_SCANCODE_X]) {
    theta.z -= 0.05;
    updateR();
  }

  if (keystate[SDL_SCANCODE_I]) { //Y
    lightPos.y -= 0.05;
  }
  if (keystate[SDL_SCANCODE_K]) {
    lightPos.y += 0.05;
  }
  if (keystate[SDL_SCANCODE_J]) { //X
    lightPos.x -= 0.05;
  }
  if (keystate[SDL_SCANCODE_L]) {
    lightPos.x += 0.05;
  }
  if (keystate[SDL_SCANCODE_U]) { //Z
    lightPos.z += 0.05;
  }
  if (keystate[SDL_SCANCODE_O]) {
    lightPos.z -= 0.05;
  }


  while (theta.x < 0) {
    theta.x += 2*M_PI;
  }
  while (theta.y < 0) {
    theta.y += 2*M_PI;
  }
  while (theta.z < 0) {
    theta.z += 2*M_PI;
  }
  while (theta.x > M_PI*2) {
    theta.x -= 2*M_PI;
  }
  while (theta.y > M_PI*2) {
    theta.y -= 2*M_PI;
  }
  while (theta.z > M_PI*2) {
    theta.z -= 2*M_PI;
  }
}
